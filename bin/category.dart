import 'product.dart';

class category {
  Map getCategory(String x) {
    if (x == '1') {
      return recommendedMenu;
    } else if (x == "2") {
      return coffee;
    } else if (x == "3") {
      return tea;
    } else if (x == "4") {
      return milkCocoaCaramel;
    } else if (x == "5") {
      return proteinShake;
    } else if (x == "6") {
      return soda;
    }
    return coffee;
  }

  Map recommendedMenu = {
    1: product().expresso,
    2: product().blackCoffee,
    3: product().ginger,
    4: product().matchaLatte
  };
  Map coffee = {
    1: product().expresso,
    2: product().blackCoffee,
    3: product().doubleExpresso,
    4: product().americaSoda
  };
  Map tea = {
    1: product().chrysanthemum,
    2: product().ginger,
    3: product().matchaLatte,
    4: product().blackTea
  };
  Map milkCocoaCaramel = {
    1: product().milk,
    2: product().caramelMilk,
    3: product().brownSugar,
    4: product().pinkMilk
  };
  Map proteinShake = {
    1: product().matchaProtein,
    2: product().cocoaProtein,
    3: product().strawberryProtein,
    4: product().milkProtein
  };
  Map soda = {
    1: product().icePepsi,
    2: product().powerOfTaoSoda,
    3: product().plumSoda,
    4: product().ice
  };
}
