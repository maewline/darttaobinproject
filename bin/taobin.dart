import 'dart:io';

import 'main.dart';

int total = 0;
String select = "";
String? sweet;
String? productSelect;
String? type = "";
String? paymentMethod = "";
String tubeScum = "";

class taobin {
  bool selectOption(List list) {
    productSelect = list[0];
    if (list[1] != null) {
      print("1.ร้อน ราคา: ${list[1]}");
      type = "(ร้อน)";
    }
    if (list[2] != null) {
      print("2.เย็น ราคา: ${list[2]}");
      type = "(เย็น)";
    }
    if (list[3] != null) {
      print("3.ปั่น ราคา: ${list[3]}");
      type = "(ปั่น)";
    }
    print("ยกเลิกกด 0");
    select = takeInput();
    if (select == '0') {
      return false;
    } else {
      total = list[int.parse(select)];
      sweet = selectSweet(list);
      takeTubeScum();
      if (showStatus() == false) {
        return false;
      }
    }
    return true;
  }

  String selectSweet(List list) {
    if (list.length > 4) {
      for (var i = 4, j = 1; i < list.length; i++, j += 1) {
        print("$j.${list[i]}");
      }
      select = takeInput();
      if (select == '1') {
        return "ไม่ใส่น้ำตาล";
      } else if (select == '2') {
        return "หวานน้อย";
      } else if (select == '3') {
        return "หวานพอดี";
      } else if (select == '4') {
        return "หวานมาก";
      } else if (select == '5') {
        return "หวานที่สุดในสามโลก";
      }
    }
    return "";
  }

  void takeTubeScum() {
    print("\nรับหลอดไหมครับ รับกด 1 ไม่รับกด 2");
    String tube = takeInput();
    print("\nรับฝาไหมครับ รับกด 1 ไม่รับกด 2");
    String scum = takeInput();
    if (tube == "1") {
      tubeScum = "รับหลอด ";
    }
    if (scum == "1") {
      tubeScum += "รับฝา";
    }
    if (tubeScum == "") {
      tubeScum = "ไม่รับหลอด ไม่รับฝา";
    }
  }

  bool showStatus() {
    print(
        "$productSelect$type $tubeScum ระดับความหวาน: $sweet ราคา: $total บาท");
    if (payment()) {
      print(
          "$productSelect$type $tubeScum $sweet ราคา: $total บาท การชำระเงิน: $paymentMethod");
      print("ชำระเงินสำเร็จ");
      print("สินค้าจะได้ภายใน 5 วินาทีครับ");
      sleep(Duration(seconds: 5));
      print("\nรับสินค้าด้านล่างได้เลยครับ");
      return true;
    } else {
      return false;
    }
  }

  String takeInput() {
    stdout.write("\nโปรดเลือกหมายเลขที่ท่านต้องการ: ");
    select = stdin.readLineSync()!;
    return select;
  }

  bool payment() {
    print("กรุณาเลือกวิธีการจ่ายเงิน");
    print("1.เงินสด");
    print("2.สแกน QR");
    print("3.เต่าบินเครดิต");
    print("4.ใช้/คูปอง");
    print("5.E-wallet");
    print("6.โปรโมชั่นอื่นๆ");
    print("ยกเลิกกด 0");
    String input = takeInput();
    if (input == "1") {
      paymentMethod = "เงินสด";
      return true;
    } else if (input == "2") {
      paymentMethod = "สแกน QR";
      return true;
    } else if (input == "3") {
      paymentMethod = "เต่าบินเครดิต";
      return true;
    } else if (input == "4") {
      paymentMethod = "ใช้/คูปอง";
      return true;
    } else if (input == "5") {
      paymentMethod = "E-wallet";
      return true;
    } else if (input == "6") {
      paymentMethod = "โปรโมชั่นอื่นๆ";
      return true;
    } else if (input == "0") {
      return false;
    }
    return false;
  }
}
