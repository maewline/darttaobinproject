import 'dart:io';
import 'category.dart';
import 'taobin.dart';

void main() {
  mainMenu();
}

void mainMenu() {
  while (true) {
    listMenu();
    String selectMenu = takeInput();
    if (selectCondition(selectMenu) == false) {
      continue;
    }
    if (anythingElse() == false) {
      break;
    }
  }
}

bool selectCondition(String selectMenu) {
  if (selectMenu == "1") {
    print("\nเมนูแนะนำ");
    if (process(selectMenu) == false) {
      return false;
    }
  } else if (selectMenu == "2") {
    print("\nหมวดหมู่กาแฟ");
    if (process(selectMenu) == false) {
      return false;
    }
  } else if (selectMenu == "3") {
    print("\nหมวดหมู่ชา");
    if (process(selectMenu) == false) {
      return false;
    }
  } else if (selectMenu == "4") {
    print("\nหมวดหมู่นม โกโก้ และคาราเมล");
    if (process(selectMenu) == false) {
      return false;
    }
  } else if (selectMenu == "5") {
    print("\nหมวดหมู่โปรตีนเชค");
    if (process(selectMenu) == false) {
      return false;
    }
  } else if (selectMenu == "6") {
    print("\nหมวดหมู่โซดาและอื่นๆ");
    if (process(selectMenu) == false) {
      return false;
    }
  }
  return true;
}

bool anythingElse() {
  stdout.write("\nต้องการรับอะไรเพิ่มเติมไหมครับ (y/n): ");
  String ans = stdin.readLineSync()!;
  if (ans == "y") {
    return true;
  } else if (ans == "n") {
    print("ขอบคุณที่ใช้บริการครับ");
    return false;
  }
  return true;
}

bool process(String selectMenu) {
  category().getCategory(selectMenu).forEach((key, value) {
    print("$key.${value[0]}");
  });
  print("ย้อนกลับกด 0");
  String option = takeInput();
  if (option == "0") {
    return false;
  } else {
    if (taobin().selectOption(
            category().getCategory(selectMenu)[int.parse(option)]) ==
        false) {
      return false;
    }
  }
  return true;
}

String takeInput() {
  stdout.write("\nโปรดเลือกหมายเลขที่ท่านต้องการ: ");
  select = stdin.readLineSync()!;
  return select;
}

void listMenu() {
  print("กรุณาเลือกประเภทเครื่องดื่ม");
  print("1.เมนูแนะนำ");
  print("2.กาแฟ");
  print("3.ชา");
  print("4.นม โกโก้ และคาราเมล");
  print("5.โปรตีนเชค");
  print("6.โซดาและอื่นๆ");
}
